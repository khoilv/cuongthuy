<footer>
    <div class="wrap">
        <div class="f_left">
            <img src="{!!Asset('public/images/logo-2.png')!!}" alt="cuongthuy.vn">
            <div>
                <h2>BẢN QUYỀN THUỘC cuongthuy.vn</h2>
                <p>Địa chỉ : Số 31 Vương Thừa Vũ - Thanh Xuân - Hà Nội<br>Tell : 097 123 123 <br>Mail : laihuycuong1812@gmail.com </p>
            </div>
        </div>
        <p class="f_right">Tổng số lượt truy cập : 2000000</p>
        <div class="clear"></div>
    </div>
</footer>
@section('javascript')
<script type="text/javascript">
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>
@yield('javascript')